<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    // 'google' => [
    //     'client_id' => '223365214013-paiuh6n31r2ge0t046qtv5md10hfp1sq.apps.googleusercontent.com',
    //     'client_secret' => 's4gRhR9w5kganhfk_2YVeLZ_',
    //     'redirect' => 'http://dashboard.dreyfi.com/auth/google/callback',
    // ],

    'google' => [
        'client_id' => '223365214013-paiuh6n31r2ge0t046qtv5md10hfp1sq.apps.googleusercontent.com',
        'client_secret' => 's4gRhR9w5kganhfk_2YVeLZ_',
        'redirect' => 'http://tasks.dashboard.com/auth/google/callback',
    ],

];
