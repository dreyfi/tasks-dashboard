<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-md-4 control-label">{{ 'Title' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="title" type="text" id="title" value="{{ $task->title or ''}}" >
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ $task->description or ''}}</textarea>
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('user') ? 'has-error' : ''}}">
    <label for="user" class="col-md-4 control-label">{{ 'User' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="user" type="text" id="user" value="{{ $user or ''}}" readonly="readonly">
        {!! $errors->first('user', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('priority') ? 'has-error' : ''}}">
    <label for="priority" class="col-md-4 control-label">{{ 'Priority' }}</label>
    <div class="col-md-6">
        <select name="priority" class="form-control" id="priority" >
    @foreach (json_decode('{"one":"1","two":"2","three":"3","four":4,"five":5}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($task->priority) && $task->priority == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
        {!! $errors->first('priority', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-4 control-label">{{ 'Status' }}</label>
    <div class="col-md-6">
        <select name="status" class="form-control" id="status" >
            @foreach (json_decode('{"done":"done","todo":"todo","proccessed":"proccessed"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($task->status) && $task->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('user') ? 'has-error' : ''}}">
    <label for="user" class="col-md-4 control-label">{{ 'Attachment' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="attachment" type="file" id="attachment" value="{{ $task->attachment or ''}}" >
        {!! $errors->first('attachment', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($files)) 
    <div class="form-group">
        <label for="user" class="col-md-4 control-label">{{ 'Files' }}</label>
        <div class="col-md-6" style="padding: 8px;">
        <ul>
            @foreach($files as $file) 
                <li><a download="{{$file}}" href="/file/{{$file}}">{{$file}}</a></li>
            @endforeach
        </ul>
        </div>    
    </div>
@endif
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
