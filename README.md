# Tasks Dashboard #

A aplicação pode ser testada no endereço [dashboard.dreyfi.com](http://dashboard.dreyfi.com).

### Tecnologias ###

* Laravel 5.4
* PHP 7.0.10
* Mysql 5.7.14

### Instalação ###

Para instalar o projeto, basta criar um virtual host em seu apache apontando para o diretório /public e
fornecer um banco de dados Mysql. Para facilitar este passo, o projeto foi desenvolvido com Wamp Server 3.0.6.

Através do prompt do Windows, com o PHP Composer instalado no sistema, navegue até o root da aplicação e execute o comando [php composer update], para carregar as dependências citadas em composer.json.

### Configuração ###

As credenciais de banco de dados devem ser informada no cabeçalho do arquivo .env, que está localizado no root da aplicação. Neste arquivo também deve ser informada a url da aplicação para o parâmetro APP_URL.

A url da aplicação deve ser informada também no arquivo /config/services.php, no parâmetro redirect do index "Google".

Através do prompt do Windows, navegue até o root da aplicação e execute o comando [php artisan migrate] para executar as migrações no novo banco de dados.

### Credenciais do Amazon S3 ###

No arquivo .env as credenciais devem ser definidas como a seguir:

S3_KEY=AKIAJIN5WYNESOUDZGKA 

S3_SECRET=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 

S3_BUCKET=tasksdashboard 

S3_REGION=sa-east-1 

### Credenciais do Elasticsearch ###

Assim como para o S3, informe os dados do Elasticserach no arquivo .env como a seguir:

ELASTICSEARCH_INDEX=tasks 

ELASTICSEARCH_HOST=http://localhost:9200

### Notas ###

Por alguma razão ainda não definida, o login pelo Google no servidor [dashboard.dreyfi.com](http://dashboard.dreyfi.com) algumas vezes falha na primeira
tentativa, por isso, se esta falha ocorrer, por favor, tente novamente. Isso não acontece localmente. Estou investigando o problema.

No servidor de homologação [dashboard.dreyfi.com](http://dashboard.dreyfi.com) não está publicado o recurso de indexação no Elasticsearch, por limitações de plano de hospedagem, mas isso pode ser testado localmente.

Não é possível rodar o mecanismo de login do Google se sua url de callback for um endereço de ip local ou "localhost".
Seu virtualhost deve atender a um dns fake, equivalente ao exemplo: http://tasks.dashboard.com.

Seu PHP deve ter como complemento um certificado [cacert.pem](http://curl.haxx.se/ca/cacert.pem). Para isso, crie o diretório \CAMINHO PARA SEU PHP\php7.0.10\ext\ssl, adicione o arquivo do link cacert.pem então no fim do seu arquivo php.ini adicione: curl.cainfo = "\CAMINHO PARA SEU PHP\php7.0.10\ext\ssl\cacert.pem".

No root do repositório adicionei o arquivo Relatorio_das_etapas, onde esclareço como foi o desenvolvimento deste projeto.