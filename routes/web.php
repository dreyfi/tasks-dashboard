<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/home', [
// 	'middleware' => 'App\Http\Middleware\SocialiteRedirectToProviderMiddleware:google',
// 	'uses' => 'HomeController@index' 
// ]);


Auth::routes();

Route::get('auth/google', 'GoogleController@redirectToProvider')->name('google.login');
Route::get('auth/google/callback', 'GoogleController@handleProviderCallback');

Route::get('/home', 'TasksController@index')->name('home');

Route::resource('task/tasks', 'TasksController');

Route::group(['prefix'=>'task', 'middleware' => 'auth'], function() {
    Route::get('/consumer', 'TasksController@attachmentS3Consumer');
});

Route::get('file/{filename}', 'FileController@getFile')->where('filename', '^[^/]+$');

Route::group(['prefix'=>'elasticsearch', 'middleware' => 'auth'], function() {
    Route::get('/test', 'TasksController@elasticsearchTest');
});