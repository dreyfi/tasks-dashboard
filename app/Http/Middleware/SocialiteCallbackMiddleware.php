<?php
namespace App\Http\Middleware;
use Closure;
use Laravel\Socialite\Facades\Socialite;
class SocialiteCallbackMiddleware
{
	public function handle($request, Closure $next, $providerDriver = null)
	{
		$socialite = Socialite::driver($providerDriver)->user();

		return $next($request);
	}
}