<?php
namespace App\Http\Middleware;
use Closure;
use Laravel\Socialite\Facades\Socialite;
class SocialiteRedirectToProviderMiddleware
{
	public function handle($request, Closure $next, $providerDriver = null)
	{
		/**
		 * Se o user está logado, redireciona
		 */
		if (auth()->check()) {
			return redirect('/');
		}
		
		/**
		 * Caso contrário, redireciona para a página de login do provider
		 */
		return Socialite::driver($providerDriver)->redirect();
		
	}
}