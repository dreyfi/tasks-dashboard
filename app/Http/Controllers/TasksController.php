<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Elasticsearch\ClientBuilder;
use Elastica\Client as ElasticaClient;

class TasksController extends Controller
{    
    protected $elasticsearch;
    protected $elastica;

    public function __construct()
    {
        // Define o cliente Elasticsearch-php 
        $this->elasticsearch = ClientBuilder::create()->build();
        $this->middleware('auth');

        // Cria o cliente Elastica
        $elasticaConfig = [
            'host' => 'localhost',
            'port' => 9200,
            'index' => 'tasks'
        ];

        $this->elastica = new ElasticaClient($elasticaConfig);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $user = \Auth::user()->name;

        if (!empty($keyword)) {
            $tasks = Task::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('user', 'LIKE', "%$keyword%")
                ->orWhere('priority', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $tasks = Task::paginate($perPage);
        }

        return view('tasks.index', compact('tasks','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('tasks.create', ['user'=>\Auth::user()->name]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();
 
        $attachment = $request->file('attachment'); 
        if($attachment) {       
            $attchmentName = time().".".$attachment->getClientOriginalExtension();
            $destinationPath = storage_path('/attachments');

            $attachment->move($destinationPath, $attchmentName);
            $requestData["attachment"] = $attchmentName;
            $requestData["status"] = 'todo';
        }

        Task::create($requestData);

        return redirect('task/tasks')->with('flash_message', 'Task added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $task = Task::findOrFail($id);
        $user = \Auth::user()->name;
        $files = explode(";",$task->attachment);

        return view('tasks.show', compact('task', 'user','files'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $user = \Auth::user()->name;
        $files = explode(";",$task->attachment);

        return view('tasks.edit', compact('task', 'user','files'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();
        
        $task = Task::findOrFail($id);

        $attachment = $request->file('attachment');
        if($attachment) {
            $attchmentName = time().".".$attachment->getClientOriginalExtension();
            $destinationPath = storage_path('/attachments');
    
            $attachment->move($destinationPath, $attchmentName);
            $requestData["attachment"] = $attchmentName;
        }

        if($requestData["status"] == 'done') {
            $requestData["status_user"] = \Auth::user()->name;

            // Indexa no elastic search
            $task->searchable();

            // TODO: INDEXAR NO ELASTIC SEARCH
        }

        $task->update($requestData);

        return redirect('task/tasks')->with('flash_message', 'Task updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Task::destroy($id);
        return redirect('task/tasks')->with('flash_message', 'Task deleted!');
    }

    public function attachmentS3Consumer(Request $request) 
    {
        // Obtém a lista de tasks a serem processadas
        $list = Task::where('status', 'todo')->get();
        $s3 = Storage::disk('s3');

        // Itera sobre os itens
        foreach($list as $task) {

            if($task->attachment) {
                // Faz o upload  
                $s3->putFileAs('attachments', new File(storage_path("/attachments/{$task->attachment}")), $task->attachment);
                
                // Define a task como proccessed
                Task::where('id', $task->id)->update(['status'=>'proccessed']);
            }
        }
        print("done");
    }

    // Testa a conexão com o cliente elasticsearch 
    public function elasticsearchTest() {

        print("testing...");
        dump($this->elasticsearch);

        // $params = [
        //     'index' => 'tasks',
        //     'id' => 'AV-mpkVA5WeakH8k8wz0',
        //     'type' => '1'
        // ];

        // $response = $this->elasticsearch->get($params);
        // dump($response);
    }
}
