<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller {
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getFile($filename)
    {

    	// $path = storage_path().'/docs/'.$filename;

        return response()->download(storage_path("/attachments/".$filename), null, [], null);
    }
}
