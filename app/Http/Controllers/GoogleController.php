<?php

namespace App\Http\Controllers;

use Socialite;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\User;

class GoogleController extends Controller
{
    /**
     * Redireciona para a página de login do google
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtém as informações do usuário
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        $state = $request->get('state');
        $request->session()->put('state',$state);
        
        $user = Socialite::driver('google')->user();

        if(\Auth::check()==false){
            session()->regenerate();
        }  

        $authUser = $this->findOrCreateUser($user);

        \Auth::login($authUser, true);
        //return redirect($this->redirectTo);
        return redirect('/task/tasks');
    }

    public function findOrCreateUser($user)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider_id' => $user->id
        ]);
    }
}