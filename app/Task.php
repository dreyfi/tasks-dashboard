<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Laravel\Scout\Searchable;

class Task extends Model
{
    use Searchable;
    
    protected $table = 'tasks';
    protected $primaryKey = 'id';

    protected $fillable = ['title', 'description', 'user', 'priority', 'status', 'attachment','status_user'];  
}
